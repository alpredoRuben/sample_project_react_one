import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import warna from './src/styles/colors';

import LoggedOut from './src/screens/LoggedOut';
import Login from './src/screens/Login';

export default class App extends Component {
  render() {
    return (
      <Login />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
