import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import warna from '../styles/colors';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Easing,
    Animated,
} from 'react-native';


export default class Notification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            positionValue: new Animated.Value(60),
        }
        this.closeNotification = this.closeNotification.bind(this);

        this.animateNotification = this.animateNotification.bind(this);
    }

    animateNotification(value) {
        const { positionValue } = this.state;

        Animated.timing(
            positionValue,
            {
                toValue: value,
                duration: 400,
                velocity: 3,
                tension: 2,
                friction: 8,
                easing: Easing.easeOutBack,
            }
        ).start();
    }

    closeNotification() {
        this.props.handleCloseNotification();
    }



    render() {
        const { typeNotif, firstMessageNotif, secondMessageNotif, showNotification } = this.props;

        const { positionValue } = this.state;

        showNotification ? this.animateNotification(0) : this.animateNotification(60);

        return (
            <Animated.View style={[{ transform: [{ translateY: positionValue }] }, scopes.wrapper]}>

                <View style={scopes.notification_content}>

                    <Text style={scopes.error_type}> {typeNotif} :</Text>
                    <Text style={scopes.error_message}>{firstMessageNotif}. </Text>
                    <Text style={scopes.error_message}> {secondMessageNotif} </Text>

                </View>

                <TouchableOpacity style={scopes.button_close} onPress={this.closeNotification}>
                    <Icon name="times" size={20} color={warna.light_gray} />
                </TouchableOpacity>
            </Animated.View>
        );
    }

}

Notification.propTypes = {
    showNotification: PropTypes.bool.isRequired,
    typeNotif: PropTypes.string,
    firstMessageNotif: PropTypes.string,
    secondMessageNotif: PropTypes.string,
    handleCloseNotification: PropTypes.func,

};

const scopes = StyleSheet.create({
    wrapper: {
        backgroundColor: warna.white,
        height: 60,
        width: '100%',
        padding: 10
    },
    notification_content: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    error_type: {
        marginRight: 5,
        marginBottom: 2,
        color: warna.dark_orange,
        fontSize: 14,
        fontWeight: '700'
    },

    error_message: {
        marginBottom: 2,
        fontSize: 14,
    },

    button_close: {
        position: 'absolute',
        right: 10,
        top: 10,
    }
});