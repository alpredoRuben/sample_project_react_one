import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import warna from '../../styles/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    TouchableHighlight,
    StyleSheet,
    View
} from 'react-native';


export default class NextArrowButton extends Component {

    render() {
        const { disabled, handleNextButton } = this.props;
        const opacityStyle = disabled ? { backgroundColor: 'rgba(255,255,255,0.2)' } : { backgroundColor: 'rgba(255,255,255,0.6)' }
        return (
            <TouchableHighlight
                style={[opacityStyle, scopes.button]}
                onPress={handleNextButton}>
                <Icon
                    name="angle-right"
                    color={warna.green_dark}
                    size={32}
                    style={scopes.icon} />
            </TouchableHighlight>
        );
    }

}

NextArrowButton.propTypes = {
    disabled: PropTypes.bool,
    handleNextButton: PropTypes.func.isRequired,
};

const scopes = StyleSheet.create({
    icon: {
        marginRight: -2,
        marginTop: -2
    },

    button: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        width: 60,
        height: 60,
    }
})