import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableHighlight
} from 'react-native';
import PropTypes from 'prop-types';
import warna from '../../styles/colors';


export default class RoundedButtons extends Component {
    render() {
        const { text, textColor, background, evenOnPress, icon } = this.props;
        const backgroundColor = background || 'transparent';
        const color = textColor || warna.white;

        return (
            <View>
                <TouchableHighlight
                    style={[{ backgroundColor }, scopes.wrapper]}
                    onPress={evenOnPress} >
                    <View style={scopes.buttonTextWrapper}>
                        {icon}
                        <Text style={[{ color }, scopes.button_text]}>
                            {text}
                        </Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

RoundedButtons.propTypes = {
    text: PropTypes.string.isRequired,
    textColor: PropTypes.string,
    background: PropTypes.string,
    icon: PropTypes.object,
    evenOnPress: PropTypes.func.isRequired,
};

const scopes = StyleSheet.create({
    wrapper: {
        display: 'flex',
        borderRadius: 40,
        padding: 15,
        borderWidth: 1,
        borderColor: warna.green_metal_face,
        marginBottom: 15,
        alignItems: 'center'
    },

    button_text: {
        fontSize: 17,
        width: '100%',
        textAlign: 'center'
    },

    buttonTextWrapper: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
});