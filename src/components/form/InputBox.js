import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Icon from 'react-native-vector-icons';
import warna from '../../styles/colors';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet
} from 'react-native';


export default class InputBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            secureInput: props.inputType === 'text' || props.inputType === 'email' ? false : true,
        };

        this.toggleShowPassword = this.toggleShowPassword.bind(this);
    }

    toggleShowPassword() {
        this.setState({ secureInput: !this.state.secureInput });
    }

    render() {
        const { labelText, labelTextSize, labelColor, textColor, borderDownColor, inputType, customStyle } = this.props;

        const { secureInput } = this.state

        const fontSize = labelTextSize || 14;
        const color = labelColor || warna.white
        const borderBottom = borderDownColor || 'transparent';

        const bools = (inputType === 'password') ? true : false;

        return (
            <View style={[customStyle, scopes.wrapper]}>
                <Text style={[{ color, fontSize }, scopes.label]}>{labelText}</Text>

                {
                    inputType === 'password' ?
                        <TouchableOpacity style={scopes.show_button} onPress={this.toggleShowPassword}>
                            <Text style={scopes.show_button_text}>{secureInput ? 'Show' : 'Hide'}</Text>
                        </TouchableOpacity> : null
                }

                <TextInput autoCorrect={false}
                    style={[{ color: textColor, borderBottomColor: borderBottom }, scopes.input_box]}
                    secureTextEntry={secureInput} />
            </View>
        );
    }
}

InputBox.propTypes = {
    labelText: PropTypes.string.isRequired,
    labelTextSize: PropTypes.number,
    labelColor: PropTypes.string,
    textColor: PropTypes.string,
    borderDownColor: PropTypes.string,
    inputType: PropTypes.string.isRequired,
    customStyle: PropTypes.object,
}

const scopes = StyleSheet.create({
    wrapper: {
        display: 'flex'
    },

    label: {
        fontWeight: '700',
        marginBottom: 10,
    },

    input_box: {
        paddingTop: 5,
        paddingBottom: 5,
    },

    show_button: {
        position: 'absolute',
        right: 0,
    },

    show_button_text: {
        color: warna.white,
        fontWeight: '700',

    }
});