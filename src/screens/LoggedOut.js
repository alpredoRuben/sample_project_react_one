import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableHighlight
} from 'react-native';
import RoundedButton from '../components/buttons/RoundedButton';
import warna from '../styles/colors';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export default class LoggedOut extends Component {
    onLoginPress() {
        alert('Sign In With Facebook Button Press')
    }

    onCreateAccountPress() {
        alert('Create New Account Button Press')
    }

    onMoreOptionPress() {
        alert('More Option Button Press')
    }

    render() {
        return (
            <View style={scopes.wrapper}>
                <View style={scopes.sub_wrapper}>
                    <Image source={require('../img/logo.png')} style={scopes.image_logo} />
                    <Text style={scopes.text_title}>WELCOME TO EASYMEET</Text>
                    <RoundedButton text="Masuk Dengan Akun"
                        textColor={warna.white}
                        background={warna.green_light}
                        icon={<Icon name="sign-in" size={20} style={scopes.btn_icon} />}
                        evenOnPress={this.onLoginPress} />

                    <RoundedButton text="Buat Akun Baru"
                        textColor={warna.white}
                        background={warna.green_light}
                        icon={<Icon name="user" size={20} style={scopes.btn_icon} />}
                        evenOnPress={this.onCreateAccountPress} />

                    <TouchableHighlight
                        style={scopes.btn_more_option}
                        onPress={this.onMoreOptionPress}>
                        <Text style={scopes.btn_txt_more_option}>Opsi Lainnya</Text>
                    </TouchableHighlight>

                    <View style={scopes.term_condition}>
                        <Text style={scopes.txt_term_condition}>
                            Dengan menekan tombol Masuk Dengan Akun, Buat Akun Baru atau Opsi Lainnya,
                        </Text>
                        <Text style={scopes.txt_term_condition}>
                            Saya menyetujui persyaratan layanan EASYMEET meliputi
                        </Text>
                        <TouchableHighlight style={scopes.btn_link}>
                            <Text style={scopes.txt_term_condition}>
                                Ketentuan Pelayanan
                            </Text>
                        </TouchableHighlight>
                        <Text style={scopes.txt_term_condition}>, </Text>
                        <TouchableHighlight style={scopes.btn_link}>
                            <Text style={scopes.txt_term_condition}>
                                Kebijakan Privasi
                            </Text>
                        </TouchableHighlight>
                        <Text style={scopes.txt_term_condition}> dan </Text>
                        <TouchableHighlight style={scopes.btn_link}>
                            <Text style={scopes.txt_term_condition}>
                                Kebijakan Non Diskriminasi
                            </Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        )
    }
}

const scopes = StyleSheet.create({
    wrapper: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
    },

    sub_wrapper: {
        flex: 1,
        display: 'flex',
        marginTop: 20,
        padding: 10,
        alignItems: 'center'
    },

    image_logo: {
        width: 100,
        height: 100,
        alignItems: 'center',
        marginTop: 50,
        marginBottom: 20,
    },

    text_title: {
        fontSize: 20,
        fontWeight: '300',
        color: warna.white,
        marginBottom: 40
    },

    btn_icon: {
        color: warna.green_dark,
        position: 'relative',
        left: 20,
        zIndex: 8
    },

    btn_more_option: {
        marginTop: 15
    },

    btn_txt_more_option: {
        color: warna.white,
        fontSize: 16,
    },

    term_condition: {
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection: 'row',
        marginTop: 30,
    },

    txt_term_condition: {
        color: warna.white,
        fontSize: 14,
        fontWeight: '600'
    },

    btn_link: {
        borderBottomWidth: 1,
        borderBottomColor: warna.white
    }

});