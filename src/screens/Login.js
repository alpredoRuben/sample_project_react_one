import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import warna from '../styles/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native';

import InputBox from '../components/form/InputBox';

import NextArrowButton from '../components/buttons/NextArrowButton';
import Notification from '../components/Notification';

export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formValid: false,
        };

        this.eventCloseNotification = this.eventCloseNotification.bind(this);
    }

    handleNextButton() {
        alert('Next Button Press')
    }

    eventCloseNotification() {
        this.setState({ formValid: !this.formValid });
    }

    render() {
        const { formValid } = this.state;
        const showNotification = formValid ? false : true;

        const background = formValid ? warna.green_metal_light : warna.dark_orange;

        return (
            <KeyboardAvoidingView
                style={[{ backgroundColor: background }, scopes.wrapper]}
                behavior="padding">
                <View style={scopes.view_wrapper}>
                    <ScrollView style={scopes.view_scroll}>
                        <Text style={scopes.head_login}> MASUK KE AKUN </Text>

                        <InputBox
                            labelText='Email'
                            labelTextSize={14}
                            labelColor={warna.white}
                            textColor={warna.white}
                            borderDownColor={warna.white}
                            inputType="email"
                            customStyle={{ marginBottom: 30 }} />

                        <InputBox
                            labelText='Password'
                            labelTextSize={14}
                            labelColor={warna.white}
                            textColor={warna.white}
                            borderDownColor={warna.white}
                            inputType="password"
                            customStyle={{ marginBottom: 30 }} />
                    </ScrollView>

                    <View style={scopes.button_next}>
                        <NextArrowButton handleNextButton={this.handleNextButton} />
                    </View>

                    <View>
                        <Notification
                            showNotification={showNotification}
                            handleCloseNotification={this.eventCloseNotification}
                            typeNotif="Error"
                            firstMessageNotif="Those credential don't look right"
                            secondMessageNotif="Please try again" />
                    </View>
                </View>

            </KeyboardAvoidingView>
        );
    }

}

const scopes = StyleSheet.create({
    wrapper: {
        display: 'flex',
        flex: 1,
    },

    view_wrapper: {
        marginTop: 60,
        flex: 1
    },

    head_login: {
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: 30,
        color: warna.white,
    },

    view_scroll: {
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 30,
        flex: 1
    },

    button_next: {
        alignItems: 'flex-end',
        right: 20,
        bottom: 20,
    }
});