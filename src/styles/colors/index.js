/** Cetakan Warna */
export default {
    blue_dark: '#002171',
    blue_light: '#5472d3',
    blue_face: '#0d47a1',
    cyan_dark: '#0093c4',
    cyan_light: '#8bf6ff',
    cyan_face: '#4fc3f7',
    green_metal_dark: '#00363a',
    green_metal_light: '#428e92',
    green_metal_face: '#006064',
    green_dark: '#00251a',
    green_light: '#39796b',
    green_face: '#004d40',
    black: '#000000',
    white: '#ffffff',
    dark_orange: '#d93900',
    light_gray: '#d8d8d8',
}